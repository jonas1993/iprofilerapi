<?php

namespace App\Http\Controllers;

use App\DynamicForm;
use App\FilledForm;
use App\Segment;
use App\SegmentDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Segment::with(['creator', 'rules'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'brand_of_focus' => 'required',
            'created_by' => 'required|numeric',
            'rules' => 'required|array',
            'rules.*.type' => 'required|numeric',
            'rules.*.field' => 'required',
            'rules.*.value' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->getMessageBag()->toArray()], 200);
        }

        $now = Carbon::now();
        $segment = new Segment;

        $segment->name = $request->name;
        $segment->description = $request->description;
        $segment->brand_of_focus = $request->brand_of_focus;
        $segment->created_by = $request->created_by;
        $segment->created_at = $now;

        $segment->save();

        if (!$segment) {
            return response()->json(['errors' => ['db_error' => 'Failed to insert into database']], 200);
        }

        foreach ($request->rules as $rule) {
            $segmentRule = new SegmentDetail;
            $segmentRule->type = $rule['type'];
            $segmentRule->field = $rule['field'];
            $segmentRule->value = $rule['value'];
            $segmentRule->created_at = $now;

            $segment->rules()->save($segmentRule);
        }

        return response()->json($segment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'brand_of_focus' => 'required',
            'created_by' => 'required|numeric',
            'rules' => 'required|array',
            'rules.*.type' => 'required|numeric',
            'rules.*.field' => 'required',
            'rules.*.value' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->getMessageBag()->toArray()], 200);
        }

        $now = Carbon::now();
        $segment = Segment::find($id);

        if (!$segment) {
            return response()->json(['errors' => ['db_error' => 'Failed to find the record database']], 200);
        }

        $segment->name = $request->name;
        $segment->description = $request->description;
        $segment->brand_of_focus = $request->brand_of_focus;

        $segment->save();

        if (!$segment) {
            return response()->json(['errors' => ['db_error' => 'Failed to insert into database']], 200);
        }

        $segment->rules()->delete();
        foreach ($request->rules as $rule) {
            $segmentRule = new SegmentDetail;
            $segmentRule->type = $rule['type'];
            $segmentRule->field = $rule['field'];
            $segmentRule->value = $rule['value'];
            $segmentRule->created_at = $now;

            $segment->rules()->save($segmentRule);
        }

        return response()->json($segment, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $success = false;
        $segment = Segment::find($id);
        if ($segment) {
            $segment->rules()->delete();
            $segment->delete();
            $success = true;
        }

        return response()->json(['success' => $success]);

    }

    public function getFields()
    {
        $uniqueFields = [];
        $uniqueCreators = [];
        $uniqueBrands = [];
        $dynamicForms = DynamicForm::has('filledForms')->get();
        foreach ($dynamicForms as $dynamicForm) {
            $fieldsStr = $dynamicForm->fields;
            $fieldsArr = explode(',', $fieldsStr);
            foreach ($fieldsArr as $field) {
                if (!empty($field) && !in_array(strtolower($field), $uniqueFields)) {
                    $uniqueFields[] = strtolower($field);
                }
            }

            $creator = $dynamicForm->creator->name;
            if (!in_array(strtolower($creator), $uniqueCreators)) {
                $uniqueCreators[] = strtolower($creator);
            }

            $brand = $dynamicForm->formMetadata->brand;
            if (!in_array(strtolower($brand), $uniqueBrands)) {
                $uniqueBrands[] = strtolower($brand);
            }
        }

        return response()->json(compact('uniqueFields', 'uniqueCreators', 'uniqueBrands'));
    }

    public function getRespondents($id)
    {
        $segment = Segment::with('rules')->find($id);
        $rules = $segment->rules;
        $filledForms = FilledForm::with('dynamicForm')->get();

        if (!$rules || !$filledForms) {
            return response()->json(['success' => false]);
        }

        $filteredFormIds = [];

        foreach ($filledForms as $form) {
            $dynamicForm = DynamicForm::where(['key_name' => $form->f_id])->exists();

            if (!$dynamicForm) continue;
            $fields = explode(',', mb_strtolower($form->dynamicForm->fields));
            $values = explode(',', mb_strtolower($form->fields));

            foreach ($rules as $rule) {
                if ($rule->type === 1) { // field type
                    $index = array_search($this->simplyStr($rule->field), $fields);

                    if ($index && ($this->simplyStr($values[$index]) == $this->simplyStr($rule->value) || $this->simplyStr($rule->value) == 'all')) {
                        $filteredFormIds[] = $form->id;
                        break;
                    }
                }
                elseif ($rule->type === 2) { //date type
                    if ($form->created_at) {
                        $parts = explode(' | ', $rule->value);
                        if (count($parts) > 1) {
                            $subType = $parts[0];
                            $fromDate = '';
                            $toDate = '';
                            switch ($subType) {
                                case 'from':
                                    $fromDate = Carbon::createFromFormat('Y-m-d', $parts[1]);
                                    break;
                                case 'to':
                                    $toDate = Carbon::createFromFormat('Y-m-d', $parts[1]);
                                    break;
                                case 'range':
                                    $fromDate = Carbon::createFromFormat('Y-m-d', $parts[1]);
                                    $toDate = Carbon::createFromFormat('Y-m-d', $parts[2]);
                                    break;
                            }

                            if (!empty($fromDate) && !empty($toDate)) {
                                if ($form->created_at->gte($fromDate) && $form->created_at->lte($toDate)) {
                                    $filteredFormIds[] = $form->id;
                                    break;
                                }
                            } elseif (!empty($fromDate)) {
                                if ($form->created_at->gte($fromDate)) {
                                    $filteredFormIds[] = $form->id;
                                    break;
                                }
                            } elseif (!empty($toDate)) {
                                if (($form->created_at->lte($toDate))) {
                                    $filteredFormIds[] = $form->id;
                                    break;
                                }
                            }
                        }
                    }
                }
                elseif ($rule->type === 3) { // creator type
                    if ($form->dynamicForm && $rule->value == $form->dynamicForm->creator->name) {
                        $filteredFormIds[] = $form->id;
                        break;
                    }
                }
                elseif ($rule->type === 4) { // brand type
                    if ($form->formMetadata && $rule->value == $form->formMetadata->brand) {
                        $filteredFormIds[] = $form->id;
                        break;
                    }
                }
            }
        }

        $result = FilledForm::whereIn('id', $filteredFormIds)->with('dynamicForm', 'formMetadata')->get();

        return response()->json($result);
    }

    private function simplyStr($string)
    {
        return  trim(mb_strtolower($string));
    }
}

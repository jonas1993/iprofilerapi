<?php

namespace App\Http\Controllers;


use App\DynamicForm;
use App\FilledForm;
use App\FormMetadata;
use App\Jobs\SendEmailJob;

use App\Role;
use App\Team;
use App\TrackDistribution;
use App\TrackLink;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use JWTAuth;
use Mockery\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Nexmo\Laravel\Facade\Nexmo;
use DB;
use Mail;

class UserController extends Controller
{


    //Get Granted Table
    public function get_Granted_Table($id){


        $track = DB::table('granted_access')
            ->join( 'name_for_template','name_for_template.id', '=', 'granted_access.f_id')
            ->where('granted_access.distributor_id' ,'=',$id)
            ->get();
        return response()->json($track);
    }
    //controller for track distribution
    public function get__track_distribution() {
        $user = Auth::user();
        if ($user->roles()->where(['access' => 'administrator'])->first()) {
            $result = TrackDistribution::with('form', 'distributor', 'creator', 'form.administrator')->get();
        }
        else if ($user->roles()->where(['access' => 'team manager'])->first()) {

            $team = $user->teams()->first();
            $teamMemberIds = $team->members()->pluck('user_id')->all();
            $teamForms = TrackDistribution::with('form', 'distributor', 'creator', 'form.administrator')->whereIn('creator_id', $teamMemberIds)->get();
            $grantedFormIds = FormMetadata::with(['administrator' => function($query) use ($user) {
                $query->where(['users.id' => $user->id]);
            }])->get()->pluck('name_for_template.id');
            $granted = TrackDistribution::with('form', 'distributor', 'creator', 'form.administrator')->whereIn('form_id', $grantedFormIds)->get();
            $result = $teamForms->merge($granted);

        }
        else {
            $owned = TrackDistribution::with('form', 'distributor', 'creator', 'form.administrator')->where(['creator_id'=>$user->id])->get();
            $grantedFormIds = FormMetadata::with(['administrator' => function($query) use ($user) {
                $query->where(['users.id' => $user->id]);
            }])->get()->pluck('name_for_template.id');
            $granted = TrackDistribution::with('form', 'distributor', 'creator', 'form.administrator')->whereIn('form_id', $grantedFormIds)->get();
            $result = $owned->merge($granted);
        }
        return response()->json($result);
    }

    public function get__track_distribution_old(Request $request){

//        $formid = $request->input('formid');
//        $creatorid = $request->input('creatorid');
//        $distributorid = $request->input('distributorid');

$formid = true;
        if($formid!=false){
            $track = DB::table('name_for_template')
                ->leftjoin( 'track_distribution','name_for_template.id', '=', 'track_distribution.form_id')
                ->leftjoin('users as u', 'track_distribution.creator_id', '=', 'u.id')
                ->leftjoin('filled_forms', 'track_distribution.form_id', '=', 'filled_forms.f_id')
                ->leftjoin('users as u2', 'filled_forms.u_email', '=', 'u2.email')
                ->select('name_for_template.*', 'track_distribution.*', 'u.email', 'u.name as distributed_by', 'u2.name as administrated_by')->get();

//                ->join('users', 'track_distribution.distributor_id','=' ,'users.id')
            //            $id = DB::table('track_distribution')->where([['form_id' ,'=',$formid],['creator_id' ,'=',$creatorid]])->select('*')->get();
             return response()->json($track);
        }
//        elseif ($creatorid != false){
//
//        }
//        else{
//        }

    }
     public function set_track_distribution($formid,$creatorid,$distributorid,$openform,$submitform,$distributeform, $createForm = false){

         if($distributeform == true){
             $id = DB::table('track_distribution')->where([['creator_id' ,'=',$creatorid],['distributor_id' ,'=',$distributorid],['form_id' ,'=',$formid]])->select('id')->get();
             if(true)
             {
//                 error_log($id[0]->id);
                 $res = DB::table('track_distribution')->where('id' ,'=',$id[0]->id)->increment('numberofsent');
                 return 'incremented by distribution';
             }
             else{
                 $res = DB::table('track_distribution')->insert(['form_id' => $formid, 'creator_id' => $creatorid,'distributor_id' => $distributorid,'numberofsent'=>1]);
                 return 'Added with numberofsent';
             }
         }
         elseif ($openform == true){
             $id = DB::table('track_distribution')->where([['creator_id' ,'=',$creatorid],['distributor_id' ,'=',$distributorid],['form_id' ,'=',$formid]])->increment('numberofopen');
//             $res = DB::table('track_distribution')->where('id' ,'=',$id[0]->id);
             return 'incremented for opening';
         }
         elseif ($createForm) {
             $res = DB::table('track_distribution')->insert(['form_id' => $formid, 'creator_id' => $creatorid,'distributor_id' => $distributorid,'numberofsent'=>0]);
             return 'Create form';
         }
         else{

             $id = DB::table('track_distribution')->where([['creator_id' ,'=',$creatorid],['distributor_id' ,'=',$distributorid],['form_id' ,'=',$formid]])->increment('numberofsubmits');
//             $res = DB::table('track_distribution')->where('id' ,'=',$id[0]->id);
             return 'incremented for submitting';

         }
         //['form_id' => $formid, 'creator_id' => $creatorid,'distributor_id' => $distributorid]
    }


    //controller to send sms and email
    public function send_SmsandEmail(Request $request){
        $emailMessage = $request->input('econtent');
        $phoneNumbers = $request->input('numbers');
        $sms = $request->input('sms');
        $emails = $request->input('emails');
        $formId = $request->input('formid');
        $creatorId = $request->input('creatorid');
        $distributorId  = $request->input('distributorid');
        $emailFrom = $request->input('emailFrom');
        $emailSubject = $request->input('emailSubject');

        $isDistribute = $request->input('is_distribute');
        $link = $request->input('link');

        if ($isDistribute && $link) {
            $now = Carbon::now();
            $trackLink = new TrackLink;
            $trackLink->link = $link;
            $trackLink->email = $phoneNumbers != 'forbidden' ? $phoneNumbers : $emails;
            $trackLink->form_id = $formId;
            $trackLink->created_at = $now;
            $trackLink->updated_at = $now;
            $trackLink->save();
        }
        $track = TrackDistribution::firstOrNew(['form_id' => $formId, 'creator_id' => $creatorId, 'distributor_id' => $distributorId]);
        if ($track->numberofsent) {
            $track->numberofsent++;
        }
        else {
            $track->numberofsent = 1;
        }
        $track->save();
        if ($sms != 'forbidden'){
            UserController::sms($phoneNumbers, $sms, $formId, $creatorId, $distributorId);
            return response()->json('sent');
        }


        if($emails!='forbidden')
        {
            UserController::mail($emails, $emailMessage, $formId, $creatorId, $distributorId, $emailFrom, $emailSubject);
            return response()->json('sent');
        }

    }
    public function get_sig(Request $request){
        return response()->json($request->input('sig'));
    }


    //controller for grant access
    public function set_GrantAccess($f_id,$c_id,$d_id){
        $i = 0;
        $dd_id = explode(',',$d_id);
        while($i!=count($dd_id))
        {
            $response = DB::table('granted_access')->insert(['f_id' => $f_id, 'creator_id' => $c_id,'distributor_id' => $dd_id[$i]]);
            $i++;
        }
        return response()->json(count($response));
    }

    public function grantUserAccess($formId, $grantedUserIds) {
        $success = true;
        $ownerId = Auth::user()->getAuthIdentifier();
        if (!is_numeric($grantedUserIds)) {
            $grantedUserIdsArr = explode(',', $grantedUserIds);
        }
        else {
            $grantedUserIdsArr = [$grantedUserIds];
        }
        $form = FormMetadata::where(['u_id' => $ownerId, 'id' => $formId])->first();

        if (!$form) {
            $success = false;
        }
        else {
            // No upgrade plan for user when granted as per discussion
            /*
            foreach ($grantedUserIdsArr as $userId) {
                $user = User::find($userId);
                if ($user && $user->roles()->where(['access' => 'basic'])->first()) {
                    $user->roles()->sync(Role::where(['access' => 'administrator'])->select('id')->first());
                }
            }
            */
            $form->administrator()->sync($grantedUserIdsArr);
            $form->save();
        }

        return response()->json(['success' => $success]);
    }
    public function get_GrantedAccess($u_id){

        $f=DB::table('granted_access')->where('creator_id' ,'=',$u_id)->pluck('f_id');
        $i=0;
        $col =[];
        while($i!=count($f))
        {
            $c=0;
            $flag = 1;
            while($c!=count($col))
            {
                if($col[$c] == $f[$i]){
                    $flag = 0;
                }
                $c++;
            }
            if($flag == 1)
            {
                array_push($col,$f[$i]);
            }
            $i++;
        }
$i=0;
        $d_idlist = [];
        while($i!=count($col)) {
            $u_names =DB::table('granted_access')->where('f_id', '=', $col[$i])->select('distributor_id')->get();
            array_push($d_idlist,$u_names);
            $i++;
        }

        $i=0;
        $fullcolofemails = [];
        while($i!=count($col))
        {
            $g = 0;
            $temparr = [];
            while($g!=count($d_idlist[$i])){
                $u =DB::table('users')->where('id', '=', $d_idlist[$i][$g]->distributor_id)->pluck('email')->first();
                array_push($temparr,$u);
                $g++;
            }
            array_push($fullcolofemails,$temparr);
            $i++;
        }
        $finalcollection = [];
        array_push($finalcollection,$col);
        array_push($finalcollection,$fullcolofemails);

//                $u =DB::table('users')->where('id', '=', $u_idlist[$i][$j])->pluck('email')->first();

        return response()->json($finalcollection);
    }

    //controller to get user by email
    public function get_UserByEmail($email){
        $user=DB::table('users')->where('email', '=', $email)->select('id','name','email')->get();
        return response()->json($user);
    }
    //Controller to set filled forrm
    public function set_filledforms(Request $request){
        $f_id = $request->input('FormID');
        $type = $request->input('type');
        $signature = $request->input('signature');
        $privacy =  $request->input('radio');
        $fields =  $request->input('Inputs');
        $u_email = $request->input('UserEmail');
        $creatorid = $request->input('creatorid');
        $distributorid = $request->input('distributorid');
        $values_form = [
            'f_id' => $f_id,
            'type' => $type,
            'signature' => $signature,
            'privacy' => $privacy,
            'u_email' => $u_email,
            'fields' => $fields,
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
        ];

        $response = DB::table('filled_forms')->insert($values_form);

        $track = TrackDistribution::firstOrNew(['form_id' => $f_id, 'creator_id' => $creatorid, 'distributor_id' => $distributorid]);
        if ($track->numberofsubmits) {
            $track->numberofsubmits++;
        }
        else {
            $track->numberofsubmits = 1;
        }
        $track->save();
        $e_c=DB::table('dy_forms')->where('key_name','=',$f_id)->select('auto_acknowledge', 'auto_email_from', 'auto_email_subject')->get();
        if(count($e_c)> 0)
        {
            $e_content = $e_c[0]->auto_acknowledge;
            if($u_email!='message' && $u_email!='i-profiler'  && $u_email!='' && $u_email!=null) {
                UserController::mail($u_email, $e_content, '', '', '', $e_c[0]->auto_email_from, $e_c[0]->auto_email_subject);
            }
        }
        return response()->json('Added');
    }

    public function get_AllFilledFormsbyF_id($id)
    {
        $f_id=strval($id);
        $Form=DB::table('filled_forms')->where('f_id','=',$f_id)->get();
        return response()->json($Form);
    }
    //Controller to get a single form
    public function get_SingleFormsByID(Request $request)
    {
        $id = $request->input('formid');
        $f_id=strval($id);
        $email = $request->email;

        $user = Auth::user();

        DB::enableQueryLog();
        Log::debug($user);
        $form = DynamicForm::where(['key_name' => $f_id])->whereHas('formMetadata', function($query){
            $now = date('Y-m-d');
            $query->whereRaw('DATE_FORMAT(activation_date, "%Y-%m-%d") <= ?', [$now])
                ->whereRaw('DATE_FORMAT(inactivation_date, "%Y-%m-%d") >= ?', [$now]);
        });
        if ($user && $user->id == 1) {
            $form->whereHas('links', function($query) use ($email) {
                $query->where(['email' => $email]);
            });
        }

        $form = $form->get();
        Log::debug(DB::getQueryLog());
        DB::disableQueryLog();
        if ($user && $user->id === 1 && !empty($form)) {
            $c_id = $request->input('creatorid');
            $d_id = $request->input('distributorid');

            if($c_id != 'forbidden'){
                $track = TrackDistribution::firstOrNew(['form_id' => $f_id, 'creator_id' => $c_id, 'distributor_id' => $d_id]);
                if ($track->numberofopen) {
                    $track->numberofopen++;
                }
                else {
                    $track->numberofopen = 1;
                }
                $track->save();
            }
        }

        return response()->json($form);
    }

    public function get_SingleFormsByIDForPreview(Request $request)
    {

        $id = $request->input('formid');
        $f_id=strval($id);
        $Form = DynamicForm::where(['key_name' => $f_id])->get();

        return response()->json($Form);
    }

    //Controller to delete form
    public function delete_FormsByID($id){
        $success = false;
        if (!FormMetadata::find($id)->filledForms()->exists()) {
            DynamicForm::where(['key_name' => $id])->delete();
            FormMetadata::find($id)->delete();
            $success = true;
        };

        return response()->json(['success' => $success]);
    }

    public function getDistributeableForms()
    {
        $user = Auth::user();
        $userId = $user->id;

        if ($user->roles()->where(['access' => 'administrator'])->first()) {
            $mergedForms = FormMetadata::all();
        }
        else {
            $createdForms = FormMetadata::where(['u_id' => $userId])->get();
            $administratedForms = FormMetadata::whereHas('administrator', function ($query) use ($userId) {
                $query->where('users.id', '=', $userId);
            })->get();

            $mergedForms = $createdForms->merge($administratedForms);

            if ($user->roles()->where(['access' => 'team manager'])->first()) {
                $team = $user->teams()->first();
                $teamMemberIds = $team->members()->pluck('user_id')->all();
                $teamForms = FormMetadata::whereIn('u_id', $teamMemberIds)->get();
                $mergedForms = $mergedForms->merge($teamForms);
            }
        }

        return response()->json($mergedForms);
    }

    public function getManageableForms()
    {
        $user = Auth::user();
        if ($user->roles()->where(['access' => 'administrator'])->first()) {
            $createdForms = FormMetadata::all();
        }
        if ($user->roles()->where(['access' => 'team manager'])->first()) {
            $team = $user->teams()->first();
            $teamMemberIds = $team->members()->pluck('user_id')->all();
            $createdForms = FormMetadata::whereIn('u_id', $teamMemberIds)->get();
        }
        else {
            $createdForms = FormMetadata::where(['u_id' => $user->id])->get();

        }

        return response()->json($createdForms);
    }
    //controller to Get All Forms Names by ID
    public function get_allFormNamesByUID($id){
        $u_id=strval($id);
        $Forms=DB::table('name_for_template')->where('u_id','=',$u_id)->select('id','form_name','created_by','creation_date','inactivation_date','activation_date','brand','u_id')->get();
        return response()->json($Forms);
    }
    public function get_allFilledFormNamesByUID($id){
        $u_id=strval($id);
        $ALLID=DB::table('filled_forms')->select('f_id')->get();



        $col =[];
        $i=0;
        while($i!=count($ALLID))
        {
            $c=0;
            $flag = 1;
            while($c!=count($col))
            {
                if($col[$c] == $ALLID[$i]){
                    $flag = 0;
                }
                $c++;
            }
            if($flag == 1)
            {
                array_push($col,$ALLID[$i]);
            }
            $i++;
        }

        $a= $col;

        $i =0;
        $f=[];
        while($i<count($a)) {
            $Forms = DB::table('name_for_template')->where([['u_id', '=', $u_id], ['id', '=', $a[$i]->f_id]])->select('id', 'form_name', 'created_by', 'creation_date', 'inactivation_date', 'activation_date', 'brand')->get();
        array_push($f,$Forms);
            $i++;
        }
//        return response()->json($f);
        return response()->json($f);
    }
    public function get_allFormNamesByName($name){
        $Forms=DB::table('name_for_template')->where('form_name','=',$name)->pluck('form_name')->first();

            return response()->json($Forms);


    }

    //authentication
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = Auth::user();
        $roles = $user->roles;

        return response()->json(compact('token', 'roles', 'user'));
    }
    public function render($id){
        $users=DB::table('dy_')->where('users.id','=',$id)->get();
        return $users;
    }

    //Controllers for Dynamic Forms
    // `id`, `user_id`, `key_name`, `fields`, `sig_box`, `privacystatement`,
    // `auto_acknowledge`, `auto_communication`, `radio_status`, `radio_active`

    // $u_id,$fields,$sig_box,$privacy_option,$auto_ackn,$auto_commun
//field names of second table
    // `id`, `u_id`, `form_name`, `created_by`, `creation_date`, `purpose`, `activity_type`,
    //  `activation_date`, `inactivation_date`, `brand`, `sadasdasd`
//sequence to recieve
    //this.FormName+'/'+this.CreatedB+'/'+this.CreationDate+'/'+this.PurposeOf+'/'+this.ActivityType+'/'+this.ActivationDate
    // +'/'+this.IactivationDate+'/'+this.BrandOf;
    //$id,$fields,$sig_box,$privacystatement,$radio_status,
//$radio_active,
//$form_name,$created_by,$creation_date,$purpose,$activity_type,$activation_date,$inactivation_date,$brand,$Ackn_Email,$sms
    public function set_dyFormById(Request $request)
    {

        $id = $request->input('userid');
        $fields = $request->input('fields');
        $sig_box = $request->input('sigstatus');
        $privacystatement = $request->input('pstatement');
        $radio_status = $request->input('radiostatus');
        $radio_active = $request->input('radioactive');
        $form_name = $request->input('formname');
        $created_by = $request->input('createdby');
        $creation_date = $request->input('creationdate');
        $purpose = $request->input('purpose');
        $activity_type = $request->input('activitytype');
        $activation_date = $request->input('activationdate');
        $inactivation_date = $request->input('iactivationdate');
        $brand = $request->input('brandof');
        $Ackn_Email = $request->input('ackn_email');
        $sms = $request->input('smsmessage');
        $emailFrom = $request->auto_email_from;
        $emailSubject = $request->auto_email_subject;


        try {
            $u_id = strval($id);
            $name_form = ['u_id' => $u_id, 'form_name' => $form_name, 'created_by' => $created_by, 'creation_date' => $creation_date,
                'purpose' => $purpose, 'activity_type' => $activity_type,
                'activation_date' => $activation_date, 'inactivation_date' => $inactivation_date, 'brand' => $brand];
            $response = DB::table('name_for_template')->insert($name_form);
            $name_id = DB::table('name_for_template')->where('form_name', '=', $form_name)->pluck('id')->first();
            // $ii = response()->json($name_id);
            $k_n = strval($name_id);
            $dy_form = ['user_id' => $u_id, 'key_name' => $k_n, 'fields' => $fields, 'sig_box' => $sig_box,
                'privacystatement' => $privacystatement, 'radio_status' => $radio_status,
                'radio_active' => $radio_active, 'auto_acknowledge'=>$Ackn_Email, 'auto_communication'=>$sms,
                'auto_email_from' => $emailFrom, 'auto_email_subject' => $emailSubject];
            $response = DB::table('dy_forms')->insert($dy_form);
        }
        catch(exception $e){
            return response()->json(['error' => 'duplicate entry'], 500);
        }
        return response()->json('Form Created');
    }
    public function get_dyFormByNameAndId(){
        return response()->json('hey it is working');
    }
    public function getAll_dyFormById($id){
        $Form=DB::table('dy_forms')->where('key_name','=',$id)->get();


return response()->json($Form);
    }

    //controllers for General Table
    //`u_id`, `title`, `f_name`, `l_name`, `place_of_work`,
    //  `phone_number`, `email`, `address`, `state`, `country`, `signature`
    public function set_GeneralTable(Request $request){
        $id =  $request->input('UserID');
        $title = $request->input('Title');
        $f_name= $request->input('f_name');
        $l_name = $request->input('l_name');
        $place_of_work = $request->input('p_work');
        $phone_number =  $request->input('number');
        $email  = $request->input('email');
        $address = $request->input('address');
        $state = $request->input('state');
        $country = $request->input('selectedcountry');
        $signature = $request->input('signature');
        $u_id=strval($id);
        $G_form = ['u_id' => $u_id, 'title' => $title,'f_name' => $f_name,'l_name' => $l_name,
                    'place_of_work' => $place_of_work, 'phone_number' => $phone_number,
                    'email' => $email, 'address' => $address, 'state' => $state, 'country' => $country, 'signature' => $signature];
        $response=DB::table('general_table')->insert($G_form);

        return response()->json('Form Added');
    }
    public function get_GeneralTable(Request $request){

        $response=DB::table('general_table')->where('u_id','=',$request->input('u_id'))->select('*')->get();
        return response()->json($response);
    }

    //controller to get all form names by id
    public function get_AllFormNamesById($id){
        $u_id=strval($id);
        $FormNames=DB::table('name_for_template')->where('u_id','=',$u_id)->select('form_name')->get();
        return response()->json($FormNames);
    }
    //controller for setting duplicate form
    public function set_duplicateform($id,$oldname,$form_name,$created_by,$creation_date,$purpose,$activity_type,
    $activation_date,$inactivation_date,$brand){
        // `id`, `u_id`, `form_name`, `created_by`, `creation_date`, `purpose`, `activity_type`, `a_date`, `ia_date`
        $u_id=strval($id);
        $name_id=DB::table('name_for_template')->where([['form_name','=',$oldname ],['u_id','=',$u_id]] )->pluck('id')->first();
        $getOldForm=DB::table('dy_forms')->where('key_name','=',$name_id)->get();

        $du_name_form = ['u_id' => $u_id, 'form_name' => $form_name,'created_by' => $created_by,'creation_date' => $creation_date,
                    'purpose' => $purpose, 'activity_type' => $activity_type,
                    'activation_date' => $activation_date, 'inactivation_date' => $inactivation_date,'brand'=>$brand,'type'=>'du'];
                    $response=DB::table('name_for_template')->insert($du_name_form);

                    $id=DB::table('name_for_template')->where([['form_name','=',$form_name ],['u_id','=',$u_id]] )->pluck('id')->first();

                    $du_form = ['user_id' => $getOldForm[0]->user_id, 'key_name' => $id,'fields' =>
                    $getOldForm[0]->fields,'sig_box' => $getOldForm[0]->sig_box,
                    'privacystatement' => $getOldForm[0]->privacystatement, 'radio_status' => $getOldForm[0]->radio_status,
                    'radio_active' => $getOldForm[0]->radio_active,'type'=>'du'];
        $response=DB::table('dy_forms')->insert($du_form);
        return response()->json('Form Duplicated');
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function registerAndGrant(Request $request) {
        $email = $request->get('email');
        $password = $request->get('password');
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        $formAdminRole = Role::where(['access' => 'basic'])->first();
        $user->roles()->attach($formAdminRole);
        $formId = $request->get('form_id');
        $content = "Your account has been created <br><br> User: $email <br> Password: $password";

        $this->mail($email, $content);
        $this->grantUserAccess($formId, $user->id);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }


    //  ========================       Arslan's Code ==================================================

   public function mail($email, $content, $formId = '', $creatorId = '', $distributorId = '', $from = '', $subject = ''){
       $details['email'] = $email;
       $details['var']=$content;
       $details['mailFrom'] = $from;
       $details['mailSubject'] = $subject;
       $details['mailContent'] = $content;
       $details['form_id'] = $formId;
       $details['creator_id'] = $creatorId;
       $details['distributor_id'] = $distributorId;

       dispatch(new SendEmailJob($details));
   }

   public function sms($number,$message, $formId = '', $creatorId = '', $distributorId = ''){
        /*
       $message = Nexmo::message()->send([
           'to'   => $number,
           'from' => 'I-profiler',
           'text' => $message
       ]);
        */
       $client = new Client();
       $user =  env('ESTORESMS_USER', '');
       $pass =  env('ESTORESMS_pass', '');
       $sender = 'I-Profiler';

       if (substr($number, 0, 3) == '234' || substr($number, 0, 4) == '+234') {
           $url = "http://www.estoresms.com/smsapi.php?username=$user&password=$pass&sender=$sender&recipient=$number&message=$message&dnd=true";
       } else {
           $url = "http://www.estoresms.com/smsapi.php?username=$user&password=$pass&sender=$sender&recipient=$number&message=$message";
       }

       $result = $client->get($url);
       if (!empty($formId)) {
           $track = TrackDistribution::firstOrNew(['form_id' => $formId, 'creator_id' => $creatorId, 'distributor_id' => $distributorId]);
           if ($track->no_delivered) {
               $track->no_delivered++;
           }
           else {
               $track->no_delivered = 1;
           }
           $track->save();
       }

   }

   public function getFormUseTrack() {
        $user = Auth::user();
        if ($user->roles()->where(['access' => 'administrator'])->first()) {
            $forms = FormMetadata::with('creator', 'latestFilledForm')->withCount('filledForms')->get();
        }
       if ($user->roles()->where(['access' => 'team manager'])->first()) {
           // get all team member
           $team = $user->teams()->first();
           $teamMemberIds = $team->members()->pluck('user_id')->all();
           $teamForms = FormMetadata::with('creator', 'latestFilledForm')->withCount('filledForms')->whereIn('u_id', $teamMemberIds)->get();
           $granted = FormMetadata::with('creator', 'latestFilledForm')->withCount('filledForms')->administrator()->where(['users.id' => $user->id])->get();
           $forms = $teamForms->merge($granted);
       }
        else {
            $owned = FormMetadata::with('creator', 'latestFilledForm')->withCount('filledForms')->where(['u_id' => $user->id])->get();
            $granted = FormMetadata::with('creator', 'latestFilledForm')->withCount('filledForms')->administrator()->where(['users.id' => $user->id])->get();
            $forms = $owned->merge($granted);
        }

        return response()->json($forms);
   }

   public function getUserDetail() {
        $userId = Auth::user()->getAuthIdentifier();

        return response()->json(User::with('roles')->find($userId));
   }

   public function postUpdateUser(Request $request) {
       $userId = Auth::user()->getAuthIdentifier();

       $success = false;

       $user = User::find($userId);
       $user->name = $request->name;
       $user->organizations = $request->organization;
       $user->mobile = $request->mobile;
       $user->country = $request->country;

       if ($user->save()) {
           $success = true;
       }

       return response()->json(['success' => $success]);
   }

   public function postCreateUserAccessManager(Request $request) {
       $validator = Validator::make($request->all(), [
           'name' => 'required|string|max:255',
           'email' => 'required|string|email|max:255|unique:users',
           'password' => 'required|string|min:6',
           'plan' => ['required', Rule::in(['basic', 'premium', 'professional', 'enterprise', 'team manager'])],
           'team' => 'nullable'
       ]);

       $success = false;
       if($validator->fails()){
           return response()->json(['success' => $success, 'error' => $validator->errors()->toArray()]);
       }
       $userId = Auth::user()->getAuthIdentifier();
       $newUser = [];
       $user = User::find($userId);

       if ($user->roles()->where(['access' => 'team manager'])->orWhere(['access' => 'administrator'])->first()) {
           $existUser = User::where(['email' => $request->email])->first();
           if (!$existUser) {
               $newUser = new User;
               $newUser->name = $request->name;
               $newUser->email = $request->email;
               $newUser->password = Hash::make($request->get('password'));
               $newUser->organizations = $user->organizations;
               if ($newUser->save()) {
                   $role = Role::where(['access' => $request->plan])->select('id')->first();
                   $newUser->roles()->sync($role);

                   if ( empty($request->team) ) {
                       $newUser->teams()->detach();
                   }
                   else {
                       $team = Team::where(['id' => $request->team])->select('id')->first();
                       $newUser->teams()->sync($team);
                   }

                   $success = true;
                   $content = "Your account has been created <br><br> User: $newUser->email <br> Password: ".$request->get('password');
                   try {
                       $this->mail($newUser->email, $content);
                   }
                   catch (\Exception $e) {
                       Log::error($e->getMessage());
                   }
               }
           }
       }

       return response()->json(['success' => $success, 'user' => $newUser]);
   }

    public function postUpdateUserAccessManager(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'nullable|string|min:6',
            'plan' => ['required', Rule::in(['basic', 'premium', 'professional', 'enterprise', 'team manager'])],
            'team' => 'nullable'
        ]);

        $success = false;
        if($validator->fails()){
            return response()->json(['success' => $success, 'error' => $validator->errors()->toArray()]);
        }
        $userId = Auth::user()->getAuthIdentifier();
        $existUser = [];
        $user = User::find($userId);

        if ($user->roles()->where(['access' => 'team manager'])->orWhere(['access' => 'administrator'])->first()) {
            $existUser = User::find($request->id);
            if ($existUser) {
                $existUser->name = $request->name;
                $existUser->email = $request->email;
                if (!empty($request->password)) {
                    $existUser->password = Hash::make($request->get('password'));
                }
                $existUser->organizations = $user->organizations;
                if ($existUser->save()) {
                    if ( !($request->plan === 'team manager' && $user->roles()->first()->access !== 'administrator') ) {
                        $role = Role::where(['access' => $request->plan])->select('id')->first();
                        $existUser->roles()->sync($role);
                    }

                    if ( empty($request->team) ) {
                        $existUser->teams()->detach();
                    }
                    else {
                        $team = Team::where(['id' => $request->team])->select('id')->first();
                        $existUser->teams()->sync($team);
                    }

                    $success = true;
                    if (!empty($request->password)) {
                        $content = "Your account password has been updated <br><br> Password: " . $request->get('password');
                        try {
                            $this->mail($existUser->email, $content);
                        } catch (\Exception $e) {
                            Log::error($e->getMessage());
                        }
                    }
                }
            }
        }

        return response()->json(['success' => $success, 'user' => $existUser]);
    }

   public function getUserList()
   {
       $userId = Auth::user()->getAuthIdentifier();
       $userList = [];
       $user = User::find($userId);
       $role = $user->roles()->first();

       if ($role->access == 'administrator') {
           // show all
           $userList = User::with('roles', 'teams')->whereNotIn('id', [1])->get(['id', 'name', 'email', 'mobile', 'country']);
       }
       else if ($role->acess == 'team manager') {
           //show team
           $team = $user->teams()->first();
           $userList = User::with('roles', ['teams' => function($query) use ($team) {
                $query->find($team->id);
           }])->get();
       }

       return response()->json($userList);
   }

    public function getTeamList()
    {
        $userId = Auth::user()->getAuthIdentifier();
        $teamList = [];
        $user = User::find($userId);
        $role = $user->roles()->first();

        if ($role->access == 'administrator') {
            $teamList = Team::all();
        }

        return response()->json($teamList);
    }

    public function createTeam(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();

        $user = User::find($userId);
        $role = $user->roles()->first();
        $success = false;
        $team = [];
        $teamList = [];
        if ($role->access == 'administrator') {

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:teams',
            ]);

            if($validator->fails()){
                return response()->json(['success' => $success, 'error' => $validator->errors()->toArray()]);
            }

            $team = new Team;
            $team->name = $request->name;
            if ($team->save()) {
                $success = true;
                $teamList = Team::all();
            }
        }

        return response()->json(['success' => $success, 'team' => $team, 'teams' => $teamList]);
    }

    public function destroy($id) {
        $success = false;
        $user = User::find($id);
        if ($user) {
            if (!($user->createdForms && $user->createdFormMetadata)) {
                $user->roles()->detach();
                $user->teams()->detach();
                $user->delete();
                $success = true;
            }
            else {
                $anonyUser = User::find(1);
                if ($anonyUser) {
                    foreach ($user->createdForms as $createdForm) {
                        $createdForm->timestamps = false;
                        $anonyUser->createdForms()->save($createdForm);
                    }

                    foreach ($user->createdFormMetadata as $createdFormMatadata) {
                        $createdFormMatadata->timestamps = false;
                        $anonyUser->createdFormMetadata()->save($createdFormMatadata);
                    }

                    $user->roles()->detach();
                    $user->teams()->detach();
                    $user->createdForms()->delete();
                    $success = true;
                }

            }
        }

        return response()->json(['success' => $success]);
    }
}

<?php

namespace App\Jobs;

use App\Mail\SendEmailTest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Mail;



class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        //
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $email = new SendEmailTest($this->details);
            if (Mail::to($this->details['email'])->send($email)) {
                if (!empty($this->details['form_id'])) {
                    $track = TrackDistribution::firstOrNew(['form_id' => $this->details['form_id'], 'creator_id' => $this->details['creator_id'], 'distributor_id' => $this->details['distributor_id']]);
                    if ($track->no_deilvered) {
                        $track->no_deilvered++;
                    } else {
                        $track->no_deilvered = 1;
                    }
                    $track->save();
                }
            }
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}

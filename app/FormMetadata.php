<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FormMetadata extends Model
{
    protected $table = 'name_for_template';

    public function dynamicForm()
    {
        return $this->hasOne('App\DynamicForm', 'key_name');
    }

    public function filledForms()
    {
        return $this->hasMany('App\FilledForm', 'f_id');
    }

    public function latestFilledForm()
    {
        return $this->hasOne('App\FilledForm', 'f_id')->latest();
    }

    public function trackDistribution()
    {
        return $this->hasOne('App\TrackDistribution', 'form_id');
    }

    public function administrator()
    {
        return $this->belongsToMany('App\User', 'form_permissions', 'form_id', 'user_id')->withTimestamps();
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'u_id');
    }


}

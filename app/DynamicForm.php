<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DynamicForm extends Model
{
    protected $table = 'dy_forms';

    public function segmentDetails()
    {
        return $this->hasMany('App\SegmentDetail');
    }

    public function filledForms()
    {
        return $this->hasMany('App\FilledForm', 'f_id', 'key_name');
    }

    public function formMetadata()
    {
        return $this->belongsTo('App\FormMetadata','key_name');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function links()
    {
        return $this->hasMany('App\TrackLink', 'form_id', 'key_name');
    }
}

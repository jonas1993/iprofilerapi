<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FilledForm extends Model
{
    protected $table = 'filled_forms';

    public function dynamicForm() {
        return $this->belongsTo('App\DynamicForm', 'f_id', 'key_name');
    }

    public function formMetadata() {
        return $this->belongsTo('App\FormMetadata', 'f_id');
    }
}

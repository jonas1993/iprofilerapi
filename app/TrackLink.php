<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TrackLink extends Model
{
    protected $table = 'track_links';

    public function dynamicForm() {
        return $this->belongsTo('App\DynamicForm', 'form_id', 'key_name');
    }

    public function formMetadata() {
        return $this->belongsTo('App\FormMetadata', 'form_id');
    }
}

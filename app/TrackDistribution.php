<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TrackDistribution extends Model
{
    protected $table = 'track_distribution';
    protected $guarded = ['id'];

    public function form() {
        return $this->belongsTo('App\FormMetadata', 'form_id');
    }

    public function distributor() {
        return $this->belongsTo('App\User','distributor_id');
    }

    public function creator() {
        return $this->belongsTo('App\User','creator_id');
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function administrating()
    {
        return $this->belongsToMany('App\FormMetadata', 'form_permissions', 'user_id', 'form_id')->withTimestamps();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    public function createdForms()
    {
        return $this->hasMany('App\DynamicForm');
    }

    public function createdFormMetadata()
    {
        return $this->hasMany('App\FormMetadata', 'u_id');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team')->withTimestamps();
    }

}

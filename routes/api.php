<?php
use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
//Access-Control-Allow-Origin: *
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
header('Access-Control-Allow-Credentials: true');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['middleware' => ['jwt.verify']], function()
{
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
    Route::get('check/{id}', 'UserController@render');
    Route::post('setdyform','UserController@set_dyFormById');
    Route::get('dyformget', 'UserController@get_dyFormByNameAndId');
    Route::get('dyformgetall/{id}', 'UserController@getAll_dyFormById');
//    Route::get('setgeneraltable/{id}/{n1}/{n2}/{n3}/{n4}/{n5}/{n6}/{n7}/{n8}/{n9}/{n0}','UserController@set_GeneralTable');
    Route::post('setgeneraltable','UserController@set_GeneralTable');
    Route::post('getgeneraltable','UserController@get_GeneralTable');
//    Route::get('getallformnamesbyid/{id}','UserController@get_allFormNamesByUID');
    Route::get('setduplicateform/{id}/{d2}/{d3}/{d4}/{d5}/{d6}/{d7}/{d8}/{d9}/{brand}','UserController@set_duplicateform');
    Route::get('getallformnamesbyid/{id}','UserController@get_allFormNamesByUID');

    Route::get('get-distributeable-forms','UserController@getDistributeableForms');
    Route::get('get-manageable-forms','UserController@getManageableForms');
    Route::get('form-use-track','UserController@getFormUseTrack' );
    Route::get('user-details','UserController@getUserDetail' );
    Route::get('user-list','UserController@getUserList' );
    Route::get('team-list','UserController@getTeamList' );
    Route::post('create-team','UserController@createTeam' );
    Route::post('update-user','UserController@postUpdateUser' );
    Route::post('create-user-access-manager','UserController@postCreateUserAccessManager' );
    Route::post('update-user-access-manager','UserController@postUpdateUserAccessManager' );

    Route::get('getallformnamesbynme/{name}','UserController@get_allFormNamesByName');
    Route::get('deleteformbyid/{id}','UserController@delete_FormsByID');
    Route::get('getallfilledformsbyuserid/{f_id}','UserController@get_AllFilledFormsbyF_id');
    Route::post('getsingleformbyidfordistributer','UserController@get_SingleFormsByID');
    Route::get('getuserbyemail/{email}','UserController@get_UserByEmail');
    //Route::get('setgrantaccess/{f_id}/{c_id}/{d_id}','UserController@set_GrantAccess');
    Route::get('setgrantaccess/{formId}/{grantedUserIds}','UserController@grantUserAccess');
    Route::get('getgrantaccess/{id}','UserController@get_GrantedAccess');
    Route::get('getallfilledformnamesbyid/{id}','UserController@get_allFilledFormNamesByUID');
    Route::post('sendsmsandemail','UserController@send_SmsandEmail');
    Route::post('getsingleformbyid','UserController@get_SingleFormsByID');
    Route::post('getsingleformbyid-for-preview','UserController@get_SingleFormsByIDForPreview');
    Route::post('setfilledform','UserController@set_filledforms');
    Route::post('gettrackdistribution','UserController@get__track_distribution');
    Route::get('getotherforms/{id}','UserController@get_Granted_Table');

    Route::get('segment/get_fields', 'SegmentController@getFields');
    Route::get('segment/get_respondents/{id}', 'SegmentController@getRespondents');
    Route::delete('user/{id}', 'UserController@destroy');
    Route::apiResource('segment', 'SegmentController');
    Route::apiResource('form_permissions', 'FormPermissionController');

    Route::post('grant_new_user', 'UserController@registerAndGrant');
});

Route::post('getsig', 'UserController@get_sig');

    Route::group(['prefix' => 'cors/','middleware'=> ['cors']], function(){

    Route::post('login', 'UserController@authenticate');
    Route::post('register', 'UserController@register');
    Route::get('open', 'DataController@open');

});

//  ========================       Arslan's Code ==================================================
Route::get('/sms','UserController@sms');
Route::get('/mail','UserController@mail');

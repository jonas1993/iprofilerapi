<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            'id' => 1,
            'access' => 'administrator',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'access' => 'team manager',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'access' => 'enterprise',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('roles')->insert([
            'id' => 4,
            'access' => 'premium',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('roles')->insert([
            'id' => 5,
            'access' => 'professional',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('roles')->insert([
            'id' => 6,
            'access' => 'basic',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);
        DB::table('roles')->insert([
            'id' => 99,
            'access' => 'anonymous',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);
    }
}

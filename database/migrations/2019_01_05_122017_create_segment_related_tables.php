<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('brand_of_focus');
            $table->integer('created_by');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('segment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('segment_id');
            $table->string('field');
            $table->string('value');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segment_details');
        Schema::dropIfExists('segments');
    }
}

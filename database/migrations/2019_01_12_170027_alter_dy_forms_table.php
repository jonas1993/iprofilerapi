<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDyFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dy_forms', function (Blueprint $table) {
            $table->string('auto_email_from')->nullable();
            $table->string('auto_email_subject')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dy_forms', function (Blueprint $table) {
            $table->dropColumn(['auto_email_from', 'auto_email_subject']);
        });
    }
}

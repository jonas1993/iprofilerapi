<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrackDistributionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('track_distribution', function (Blueprint $table) {
            //
            $table->timestamps();
            $table->integer('no_delivered')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('track_distribution', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->dropColumn('no_delivered');
        });
    }
}
